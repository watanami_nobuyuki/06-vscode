# 分割処方について

## 経緯

### 原因

- 😄大学病院「患者がいっぱい来てうざい。90日処方ばんばん出して次の診察まで間隔あけよ」
- 💀厚労省「90日も薬だして、患者は服薬管理できんの？残薬むだにならん？」

<BR>

### 2年前の「30日超え処方」の条件１

- 💀厚労省「==患者が服薬管理できる==って医者が認めるなら30日超えで処方していいよ」
- 💀厚労省「その代わり、医者は==この患者は服薬管理できる==ってカルテに書いてね」

<BR>

### 2年前の「30日超え処方」の条件２

- 😄誰か「じゃあ服薬管理できん場合は？」
- 💀厚労省「下記条件みたしてね」
    1. 30日以内に再診こさせる
    2. 他院に紹介しちゃう
    3. ==分割処方==で院外薬局に世話させる　←今回の改定  

<BR>

### 今回の改正の中身

- 💀厚労省「2年前に思いつきで分割処方OKにしたけど、色々つっこまれたから整備しましたー」
- 💀厚労省「思い切って処方箋分けちゃわね？この際だし処方せん漢字にしよーぜ」

<BR>

## 使用期限について(以下、90日分を30日×3回の分割処方として)

- 😄誰か「3枚あるけど、それぞれの有効期限はどうなんの？」
- 💀厚労省「1枚目が==4日以内==に調剤されてれば2枚目、3枚目も有効とみなす  」
- 😄誰か「じゃあ2枚目を1年後に持ってきてもOK？」  
- 💀厚労省「調剤の期限は処方日数＋使用期限の4日。なので90日＋4日が上限なので、、  
    - 💀厚労省「2枚目を95日目にもってきた⇒アウト  」
    - 💀厚労省「2枚目を93日目に持ってきた⇒2日分だけ調剤  」

## 紙の扱いについて

- 😄誰か「毎回全部持ってくの？  」
- 💀厚労省「もち」
- 😄誰か「院外薬局が処方箋預かるのはあり？  」
- 💀厚労省「了承得たんならえーよ」
- 😄誰か「なくしたら？」  
- 💀厚労省「さあ？」

## 院外薬局的に（30日×3分割として）

- 調剤料とか全部1/3しかもらえんらしい  
- 30日分なくなるころに電話かける  
- 郵送してるところはそれもありらしい  
- 2枚目3枚目の調剤時には病院に情報提供or残薬確認して薬調整  
    ⇒医者に必ず電話くるのでうっとうしい。  
- 患者の囲い込みできるからドラッグストアは嬉しいけど、門前薬局はうれしくない  
    病院に電話くるから医者もうれしくない