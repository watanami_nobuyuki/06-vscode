# 摂食機能療法について

## 有償

```mermaid
graph LR
1(リハ処方<br>算定区分で,<b>摂食機能療法</b> <br>訓練内容で摂食嚥下)
-->2[リハ予約]

2[リハ予約]
-->3[リハ受付<br>実施で摂食機能療法1,2]


```
### :smile:メリット  
医者が算定区分を選びやすい  
実施情報として電カルに飛んでくる算定区分が「摂食機能療法」  
### :cry:デメリット  
算定区分を追加する場合は有償。。。  



***
<br><br>
## 無償  
```mermaid
graph LR
1(リハ処方<br>算定区分で<B>脳血管など</B> <br>訓練内容で摂食嚥下)-->2[リハ予約]

2[リハ予約]-->3[リハ受付<br>実施で摂食機能療法1,2]
```
### :smile:メリット  
    保守範囲内  

### :cry:デメリット  
    医者が算定区分を選びづらい（？）  
    実施情報として電カルに飛んでくる算定区分が「従来算定」

<br><br>
## その他
:question:算定区分を増やさない場合のリハメイト側の操作がイメージできない。。  
療法士さんならわかる？  
どの算定区分を選ぶか？起算日はどうするか？それによって6単位越などの算定チェックが正常に動作するか？）  