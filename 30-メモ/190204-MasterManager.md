# 仕様書

## プロジェクト図

```mermaid
    graph TD
        A[Application]
        B[Infrastructure]
        C[Domain]
        D[DAOWrapper]
        A-->B
        A-->C
        B-->D
        B-->C
```

## クラス

### Applicationプロジェクト

- Formフォルダ
    各フォームを格納

- Recsharpフォルダ
    イメージファイルを格納。アイコンとか

### DAOWrapperプロジェクト

- DBアクセスするところのラッパー
    →ほかのSLNでも使えるようにね！

### Domainプロジェクト

- 入力の妥当性確認や計算(見積なら合計金額や税率計算)

### Infrastructureプロジェクト

- SQLとかね

### TESTADOWrapperプロジェクト

- MS testを使ってUnitTest作る
