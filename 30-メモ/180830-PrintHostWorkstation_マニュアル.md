# PrintHostWorkstation_マニュアル

## 機能について

- 電子カルテないで印刷する帳票が正常に印刷されるかを、電子カルテを起動せずに確認できます。(下記使用例)

    1. 院外処方箋が手元プリンタからA5で印刷されるか
    2. A5のトレイ設定が間違っていないか
    3. リストバンドが正常に印刷されるか
    4. 検査ラベルが正常に印刷されるか(電子カルテから印刷する場合に限ります)
    5. 輸血依頼箋やリハ処方箋など、印刷先プリンタが院内固定のものの印刷が正常におこなえるか

- 印刷処理を行った端末がBaseWorkstation上でどのように設定されているか確認できます。
    1. 入外、診療科、病棟
    2. 受付モード、ブロック
    3. 処方・注射・処置の実施場所
    4. スクリーンセイバー起動までの時間

## 機能イメージ

- ExWorkstation.mdbの情報をもとに指定されたプリンタへ印刷を行います。
```mermaid
    graph TD
        h[端末設定情報]
        a[印刷ツール]
        g[帳票]
        subgraph 起動した端末の情報
            e[ホスト名]
            f[登録されているプリンタ]
        end
        
        subgraph C:\egmain-ex\data\Print配下
            d[帳票定義体]
        end

        subgraph C:\egmain-ex\data\mdb\ExWorkstation.mdb
            b[Base_PrintForm]
            c[Base_Workstation]
        end

        b-.->a
        c-.->a
        f-.->a
        e-.->a
        a--印刷-->h
        d--印刷-->g
        a-->d
```

## 各ファイルについて


|ファイル名|用途|
|:---|:---|
|PrintHostWorkstation.xlsm|自動印刷ツール本体です。|
|AutoPrint.vbs|自動印刷をキックするVBスクリプトです。|
|PrintHostWorkstation_簡易マニュアル.pdf|このマニュアルです。|
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>
<BR>

## 初期設定 概要

```mermaid
        graph LR
            subgraph 最新化した端末にて
                a[1.外来-病棟端末ごとに印刷する帳票を選定]
                a -.-> c
                subgraph PrintHostWrokstation.xlsm Settingシートにて
                    b[2.Base_PrintFormの取り込み] --> c[3.印刷の定義]
                end
                
                subgraph 端末管理台帳.xlsmにて
                    d[4.端末管理台帳にAutoPrint.vbsを定義]
                end
                
                c-->d
            end
```
<BR>

## 初期設定 詳細な手順

### 外来-病棟端末ごとに印刷する帳票を選定
- サンプル
    |入外|印刷物例|
    |:---|:---|
    |外来端末|院外処方箋、検体ラベル、予約票<BR>PrinterNoが分かれているもの（輸血依頼箋など）
    |病棟端末|リストバンド、検体ラベル<BR>PrinterNoが分かれているもの（輸血依頼箋など）
    ※上記とは別に、端末設定をA4でデフォルトプリンタから印刷します。
<BR>

### Base_PrintFormの取り込み

**以下、PrintHostWorkstation.xlsmのSettingシートにて**

- PrintFormとりこみボタンを押下
    →当端末のBase_PrintFormを取り込みます
<BR>

### 印刷の定義

**以下、PrintHostWorkstation.xlsmのSettingシートにて**

- 下記に従い、印刷対象設定をA列の対象行に定義します。

    |フラグ|印刷対象|
    |:---|:---|
    |0|Base_WorkStaionのNyugaiDiv=0の場合に出力
    |1|Base_WorkStaionのNyugaiDiv=1の場合に出力
    |2|Base_WorkStaionのNyugaiDiv=2の場合に出力
    |1,2|Base_WorkStaionのNyugaiDiv=1、2の場合に出力
    |Option|印刷するかメッセージボックスを表示<BR>==AutoPrintからの起動時は使用不可。メッセージボックスで処理が止まります==

<BR>

- 下記に従い、印字向きをB列の対象行に定義します。
  |フラグ|向き|
    |:---|:---|
    |1以外|横向き（左⇒右）に印字します
    |1|縦向き（上⇒下）に印字します
  <BR>

### 端末管理台帳にAutoPrint.vbsを定義

**以下、端末管理台帳.xlsmにて**

1. [アプリ一覧]にAutoPrint.vbsを定義します。
2. [端末一覧]の起動アプリにAutoPrint.vbsを定義します。

<BR>

## 使用方法のイメージ

### フロー

```mermaid
    graph TD
        A[AutoPrint.vbs]
        B[PrintHostWrokstation.xlsmを起動]
        C[PrintHostWrokstation.xlsmを終了]
        A --印刷マクロを指定してEXCELキック--> B
        B--印刷-->C
```

### 注意点

- AutoPrint.vbsを起動すると、EXCELが起動し帳票を印刷し、EXCELが終了します。
- ホスト名が端末マスタに存在しない場合、「存在しない」とメッセージが出てEXCELが停止します。
    ==※はい・いいえの画面で止まります！==
- 帳票定義体の左上にホスト名が印字されます。
