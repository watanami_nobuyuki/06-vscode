# MARKDOWNの文法

## 強調

*Italic*
**Bold*-  
~~StrikeThrough~~
==mark==
30^2^
H~2~O
:smile:
:fa-car:
::

## リスト

- List1
  - List1-1
- List2
- List3

1. リスト１
2. リスト２
3. リスト３

## 引用

> 引用
>> 引用引用

## タスク

- [ ] test
- [ ] test

## 水平線

***

## テーブル記法

|  果物  | 値段       |
| :----- | ----: |
| ぶどう | 398円 |
| みかん | 100円 |

果物|値段
:--|--
ブドウ|390円
ringo |250円

## リンク

http://google.cojp

![](tmp/180508-古河NANDA.png)


## 図形の書き方

```mermaid
graph TD
    四角
    1[改行<br>改行]
    2(長丸)
    3((丸))
    4{ひし形}
    5>リボン]
```

```mermaid
graph TD
スタート-->実線
スタート-.->点線
スタート==>太線
スタート--経由しつつ-->実線です

```

``` mermaid
graph LR
    subgraph A
        A1-->1((b))
    end

    subgraph B
        B1-->2{b}
    end
```

``` mermaid
    gantt
    title 進捗管理
    dateFormat mmdd
    section STEP01
        工程１:a1,4/17,1d
        工程２:a2,after a1,2d
    section STEP02
        工程3:,after a2,3d
```
