# log4net

## 導入方法

1. log4net.dllを参照追加
2. いつものMylog4net.configを取り込む
(出力ディレクトリにコピーを「常にコピー」にする)
3. AssemblyInfo.csに追記

    ``` csharp
    //Log4Netの設定(Mylog4netに設定記述。動的に設定反映するためにwatch)
    [assembly:log4net.Config.XmlConfigurator(ConfigFile =@"Mylog4net.config",Watch =true)]
    ```

4. ロガー提供クラスを追加

    ``` csharp
    using log4net;      //add
    public class Log{
        private static ILog logger = null;
        public static ILog getLogger(){
            if (logger == null){
                logger = LogManager.GetLogger(Assembly.GetExecutingAssembly().FullName);
            }
            return logger;
        }
    }
    ```

5. ログ出力する

    ``` csharp
    using log4net;      //add
    static class Program{
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main(){
            ILog log = Log.getLogger();
            log.Info("===== Program Start");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            log.Info("===== Program End");
        }
    }
    ```

## 保存場所（アペンダ）

|アペンダ|出力形態|
|:--|:--|
log4net.Appender.AdoNetAppender|データベース<BR>（ADO.NETを使用）
log4net.Appender.ConsoleAppender|コンソール
log4net.Appender.ColoredConsoleAppender|コンソール<BR>（エラーレベルごとに色を指定できる）
log4net.Appender.EventLogAppender|イベント・ログ
==log4net.Appender.FileAppender==|ファイル
log4net.Appender.NetSendAppender|NetSendコマンドで、Windowsユーザーへ送付
==log4net.Appender.RollingFileAppender==|ファイル<BR>（最大値でファイル分割）
log4net.Appender.SmtpAppender|メール
log4net.Appender.TraceAppender|トレース情報として生成

## ログの書式

|値   |説明   |
|:---|:---|
%d|ログ日時の出力
%L|行番号の出力
%m|メッセージを出力
%n|改行文字の出力
%p|ログレベルの出力
%t|ログを生成したスレッドの出力
%M|ログを出力したメソッド名
%logger|ログクラスのGetLoggerメソッドの引数に渡した値

## ログの出力レベル

レベル|説明
|:---|:---|
Fatal|システム停止のような致命的なエラー
Error|問題となるエラー
Warn|注意や警告
Info|操作ログ情報
Debug|開発用デバッグ情報
