# StopWatch

- 処理時間の計測とか

## 導入方法

- 手順

    ``` csharp
    var stopWatch = new System.Diagnostics.Stopwatch();
    stopwatch.start();
    ～～～
    stopwatch.stop();
    messagebox.show(stopwatch.elapsedmilliseconds);
    TimeSpan ts = stopwatch.Elapsed;

    ts.hours
    ts.minutes
    ts.seconds
    ts.milliseconds
    stopwatch.restart();
    ```
