# Material Skin（Material Skin Updatedも）

- マテリアススキンを使える
- .net 4.5.0はProgressBarとListViewはつかえんがよ

## 導入方法

- イニシャライズ

    ``` csharp
    using MaterialSkin;
    public partial class MainForm :MaterialSkin.Controls.MaterialForm{
        private async void Form1_Load_1(object sender, EventArgs e){
            //灰色がこっち
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            //ブルーがこっち
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue800,
                Primary.Blue900,
                Primary.Blue500,
                Accent.LightBlue200,
                TextShade.WHITE
            );
    ```