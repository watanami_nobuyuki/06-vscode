# ObjectListView

## できること

- リストを、自動的に行のソートとグループ化を含むListViewに自動変換
- セル値を編集
- ドラッグアンドドロップ
- 自動グループ化（折りたたみ可能なグループを含む）
- 列は固定幅でも、最小幅でも最大幅でも、またはスペースで充填
- リストが空の場合、「リストは空です」というメッセージを表示
- ツールチップをサポート
- セル内のボタン、チェックボックス、TreeListViewの階層チェックボックス
- 任意の列の検索
- ハイパーリンクをサポート
- テキストフォント/色の変更や装飾によるホットトラッキングをサポート
- チェックボックス、単語の折り返し、縦書きのテキストを含む列ヘッダーの書式設定をサポート
- virtualリストのグループをサポート
- テキストフィルタリングを含む、フィルタリングをサポート
- セル、行、リスト全体のアニメーションをサポート
- ヘッダーを右クリックして列の選択をサポート
- Excelのような列のフィルタリングをサポート
- 標準のINotifyPropertyChangedを使用して自動更新をサポート

## 種類

### ObjectListViewコントロール

- 約1000行以下に適してる。悩んだらとりあえずこれ

### VirtualObjectListViewコントロール

- 1000万の検索結果とか大量データ用
- IVirtualListDatacsharpインターフェイスを実装する必要あり
- タイル表示は使えない
- アニメーションGIFもだめ

### FastObjectListViewコントロール

- ちょっぱや
- タイル表示も使用できません
- アニメーションGIFを表示できん

### TreeListViewコントロール

- ツリー構造のデータモデルを表示できる。

### DataListViewコントロール

- DatacsharpをIDEからプロパティで指定するだけ

### FastDataListViewコントロール

- DataListViewの速度特化型

### DataTreeListViewコントロール

- DataTreeListViewにどの行が行IDを保持し、親IDを保持しているかを伝えるだけでOK

## 導入方法

1. NugetでObjectListView.Official
2. ユーザコントロールを追加
3. designer.csに追記

    ``` csharp

    //宣言
    private BrightIdeasSoftware.ObjectListView uxObjectList;
    private BrightIdeasSoftware.OLVColumn uxOlvColumnTableName;
    private BrightIdeasSoftware.OLVColumn uxOlvColumnDescriptionName;
    private BrightIdeasSoftware.OLVColumn uxOlvColumnMdbFullName;

    //インスタンス
    this.uxObjectList = ((BrightIdeasSoftware.ObjectListView)(new BrightIdeasSoftware.ObjectListView()));
    this.uxOlvColumnTableName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
    this.uxOlvColumnMdbFullName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
    this.uxOlvColumnDescriptionName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));

    //画面に追加
    this.Controls.Add(this.uxObjectList);

    //いろいろ定義
    //
    //uxObjectList
    //
    this.uxObjectList.AllColumns.Add(this.uxOlvColumnTableName);
    this.uxObjectList.AllColumns.Add(this.uxOlvColumnDescriptionName);
    this.uxObjectList.AllColumns.Add(this.uxOlvColumnMdbFullName);
    this.uxObjectList.AllowColumnReorder = true;
    this.uxObjectList.AllowDrop = true;
    this.uxObjectList.Dock = System.Windows.Forms.DockStyle.Fill;
    //this.uxObjectList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
    //| System.Windows.Forms.AnchorStyles.Left)
    //| System.Windows.Forms.AnchorStyles.Right)));
    //this.uxObjectList.CheckBoxes = true;
    //this.uxObjectList.CheckedAspectName = "";
    this.uxObjectList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { this.uxOlvColumnTableName, this.uxOlvColumnDescriptionName, this.uxOlvColumnMdbFullName });
    this.uxObjectList.Cursor = System.Windows.Forms.Cursors.Default;
    this.uxObjectList.FullRowSelect = true;
    this.uxObjectList.HeaderWordWrap = true;
    this.uxObjectList.HideSelection = false;
    this.uxObjectList.IncludeColumnHeadersInCopy = true;
    //this.uxObjectList.Location = new System.Drawing.Point(3, 57);
    //this.uxObjectList.Size = new System.Drawing.Size(675, 439);
    this.uxObjectList.Name = "uxObjectList";
    this.uxObjectList.OverlayText.Alignment = System.Drawing.ContentAlignment.BottomLeft;
    this.uxObjectList.OverlayText.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
    this.uxObjectList.OverlayText.BorderWidth = 2F;
    this.uxObjectList.OverlayText.Rotation = -20;
    this.uxObjectList.OverlayText.Text = "test";
    this.uxObjectList.SelectColumnsOnRightClickBehaviour = BrightIdeasSoftware.ObjectListView.ColumnSelectBehaviour.Submenu;
    this.uxObjectList.ShowCommandMenuOnRightClick = true;
    this.uxObjectList.ShowGroups = false;
    this.uxObjectList.ShowHeaderInAllViews = false;
    this.uxObjectList.ShowItemToolTips = true;
    this.uxObjectList.SortGroupItemsByPrimaryColumn = false;
    this.uxObjectList.TabIndex = 36;
    this.uxObjectList.TriStateCheckBoxes = true;
    this.uxObjectList.UseAlternatingBackColors = true;
    this.uxObjectList.UseCellFormatEvents = true;
    this.uxObjectList.UseCompatibleStateImageBehavior = false;
    this.uxObjectList.UseFilterIndicator = true;
    this.uxObjectList.UseFiltering = true;
    this.uxObjectList.UseHotItem = true;
    this.uxObjectList.View = System.Windows.Forms.View.Details;
    //
        // uxOlvColumnTableName
    //
    this.uxOlvColumnTableName.AspectName = "TableName";
    this.uxOlvColumnTableName.Text = "TableName";
    this.uxOlvColumnTableName.ToolTipText = "";
    this.uxOlvColumnTableName.Width = 600;
    //
    // uxOlvColumnDescriptionName
    //
    this.uxOlvColumnDescriptionName.AspectName = "DescriptionName";
    this.uxOlvColumnDescriptionName.IsTileViewColumn = true;
    this.uxOlvColumnDescriptionName.Text = "DescriptionName";
    this.uxOlvColumnDescriptionName.IsVisible = false;
    //
    // uxOlvColumnMdbFullName
    //
    this.uxOlvColumnMdbFullName.AspectName = "MdbFullName";
    this.uxOlvColumnMdbFullName.Text = "MdbFullName";
    this.uxOlvColumnMdbFullName.IsVisible = false;
    this.uxObjectList.RebuildColumns();
    ```

4. ユーザコントロールでいろいろ動くようにする

    ``` csharp
    this.uxObjectList.RowHeight = 55;
    this.uxObjectList.SmallImageList = uxImageList;
    this.uxObjectList.EmptyListMsg = "No Data";
    this.uxOlvColumnTableName.Renderer = CreateDescribedTaskRenderer();
    this.uxOlvColumnTableName.ImageAspectName = "ImageName";
    ```

5. 値の格納

    ``` csharp
    LIST<myClass> lst = new list<myClass>();
    uxObjectList.SetObjects(lst);
    ```

6. レンダー

    ``` csharp
    //TODO:レンダー
    private DescribedTaskRenderer CreateDescribedTaskRenderer() {
        DescribedTaskRenderer renderer = new DescribedTaskRenderer();
        renderer.ImageList = this.uxImageList;
        renderer.DescriptionAspectName = "DescriptionName";
        renderer.TitleFont = new Font("Meiryo UI", 11, FontStyle.Bold);
        renderer.DescriptionFont = new Font("Meiryo UI", 9);
        renderer.ImageTextSpace = 8;
        renderer.TitleDescriptionSpace = 1;
        renderer.UseGdiTextRendering = true;
        return renderer;
    }
    ```

7. フィルタのサンプル

    ``` csharp
    /// <summary>
    /// フィルタコマンドよ
    /// </summary>
    private void RebuildFilters() {
        List<IModelFilter> filters = new List<IModelFilter>();
        if (!String.IsNullOrEmpty(this.uxFilterText.Text)) {
            char[] _splitChar = { ' ', '　' };
            String[] _arrFilterText = this.uxFilterText.Text.Split(_splitChar);
            foreach (string _filterString in _arrFilterText) {
                filters.Add(new TextMatchFilter(this.uxObjectList, _filterString));
            }
        }
    this.uxObjectList.AdditionalFilter = filters.Count == 0 ? null : new CompositeAllFilter(filters);
    }
    private void uxFilterText_TextChanged(object sender, EventArgs e) {
        this.RebuildFilters();
    }
    ```