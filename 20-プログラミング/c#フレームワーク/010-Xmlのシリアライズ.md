si# Xml.Serialization

## 前提

1. 読み書き対象のクラス（もしくはListとかいろいろ）

    ```csharp
    class Person{
        string Name{get;set;}
        string Age{get;set;}
    }
    ```

## データ書き込み

1. インスタンスとデータ

    ``` csharp
    Person person = new Person();
    person.Name = "tarou";
    person.Age = 31;
    ```

1. 出力先のストリームを作ってシリアライズ化

    ``` csharp
    using(StreamWriter _write = new StreamWriter(@"D:\test.xml",false,Encoding.UTF8)){
        XMLSerializer _serializer = new XmlSerializer(typeof(Person));
        _serializer.serialize(_write,person);
    }
    ```

## データ読み込み

1. 読込用のストリームを作ってデシリアライズ

    ``` csharp
    using (StreamReader _reader = new StreamReader(_exportPath)) {
    //デシリアライズ
        XmlSerializer serializer = new XmlSerializer(typeof(Person));
        person = (Person)serializer.Deserialize(@"D:\test.xml");
    }
    ```