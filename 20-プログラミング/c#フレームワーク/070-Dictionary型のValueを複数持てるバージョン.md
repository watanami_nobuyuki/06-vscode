# System.Collections.Specialized

- Dictionary型のValueを複数持てるバージョン

## 導入方法

- 手順

    ``` csharp
    using System.Collections.Specialized;
    NameValueCollection nvc = newNameValueCollection();
    //① キーと値の追加
    nvc.Add("キー１", "値１-１");
    nvc.Add("キー１", "値１-２");

    //② 格納してるキーの取得
    string[] keys = nvc.AllKeys;
    foreach (string s in keys) {
      Console.WriteLine(s);
    }

    //③インデックス指定でキーの取得
    Console.WriteLine(nvc.GetKey(0)); // 出力：キー１

    //④キーのすべての値を配列で取得
    string[] values1 = nvc.GetValues(0);
    string[] values2 = nvc.GetValues("キー１");

    //④キーのすべての値をカンマ区切りで取得
    string values1 = nvc(0);
    string values2 = nvc("キー１");
    ```