# コーディング規約

## グローバル変数・メンバ変数

### 基本方針は「使わない」

- スコープもくそもなくなる。引数で済ませましょう。
- メンバ変数も減らしまう

## 命名手法

### パスカル

- CanOpenPortとか
    ==※頭と単語の区切りを大文字に==

### キャメル

- canOpenPortとか
    ==※単語の区切りを大文字に==

### スネーク

- example_case
    ==※すべて小文字で複合語のスペースをアンダースコアで区切る。※==

### ハンガリアン

- strMySQLとか
    ==※頭に型名つける。使ったらダメ==

## 命名規約

### 大前提

- publicなものはパスカル（頭大文字）
- privateなものはキャメル(頭小文字)
    ※privateなメソッドもキャメル！

### コントロール

- ux+パスカル型+コントロール名

    ``` csharp
    //WinFromなら
    uxUserIdField
    uxHeaderLabel
    uxPatientDateOfBirthField
    uxSubmitCommand

    //XAMLなら
    UserIdField
    HeaderLabel
    PatientDateOfBirthField
    SubmitCommand
    ```

### 名前空間

- 会社名.ツール名.VIEWとか

    ```csharp
    INTEC.NextRecsharp.VIEW
    ```

### クラス名、関数名、構造体名

- パスカルケースかつ==動詞になるようにする==

    ``` csharp
    CanOpenPort
    ```

- インターフェースは==頭にIをつける==

    ``` csharp
    ICanOpenPort
    ```

### 列挙型（複数の名前に一連の定数を付ける）

- クラス名と一緒

    ``` csharp
    enum Days {
        Mon,
        Tue,
        Wed,
        Thu,
        Fri,
        Sat,
        Sun
    }
    ```

### プロパティ

- パスカルケースかつ名詞

    ``` csharp
    Record
    ```

### メンバ変数(クラス変数)

- アンダースコア＋キャメルケース

    ``` csharp
    _record
    ```

- 定数は末尾にConst

    ``` csharp
    _recordConst
    ```

### ローカル変数、パラメータ(引数)

- キャメルケース

    ``` csharp
    record
    ```

- 定数は末尾にConst

    ``` csharp
    recordConst
    ```

### ループに使う変数

- ネストが一つならiでOK。ネストが2つ以上なら軽く意味持たせる

    ``` csharp
    for ci = 1 to ColNum
        for ri = 1 to RowNum
    ```

### 例外クラス

- パスカルケース＋==Exception==

    ``` csharp
    NormailException
    ```

### コレクションクラス

- キャメルケース＋==Collection==

    ``` csharp
       List<string> testCollection
    ```

### デリゲートクラス

- キャメルケース＋==Delegate==

    ``` csharp
        putTestDelegate
    ```
