# LINQ集

## 要素の取得（単一）

### ElementAt(指定した位置(インデックス)にある要素を返す)

``` csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.ElementAt(2); //52返る
```

### First(最初の要素を返す)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.First();  //11返る
csharp.First(x=> x > 15);   //16返る
```

### Last(最後の要素を返す)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Last();   //8返る
csharp.Last(x=> x > 15); //21返る
```

### single(唯一の要素を返す。複数あれば例外返す)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.single(x => x > 20);   //例外変える
csharp.Last(x => x > 22); //52返る

```

## 要素の取得（複数）

### Where(条件を満たす要素をすべて返す)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Where(X => X > 20);  //52,21を返す
```

### Distinct(重複を除く)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Distinct();   //11,16,52,21,8を返す
```

### Skip(先頭から指定数省く)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Skip(3)//21,8,8を返す
```

### SkipWhile(条件を満たすまで、先頭から省く)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.SkipWhile(x => X < 20);   //52,21,8,8を返す
```

### Take(先頭から指定数取得)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Take(3);  //11,16,52を返す
```

### TakeWhile(条件を満たすまで、先頭から指定数取得)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.TakeWhile(X => x < 20);   //11,16を返す
```

## 集計

### Max(最大値を返す)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Max();   //52を返す
```

### Min(最小値を返す)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Min();   //8を返す
```

### Average(平均値を返す)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Average();   //19.3333を返す
```

### Sum(合計値を返す)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Sum();   //116を返す
```

### Count(要素数を返す)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Count(); //6を返す
```

## 判定

### All(全ての要素が条件xを満たしているか判定)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.All(x => X > 10);//Falseを返す
```

### Any(条件xを満たす要素があるかを判定)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Any(x => x > 50);//Trueを返す
```

### Contain(指定した要素が含まれているかを判定)

```csharp
var csharp = new[] { 11, 16, 52, 21, 8, 8 };
csharp.Contain(11); //Trueを返す
```

### SequenceEquals(2つのシーケンスが等しいかを判定)

```csharp
var csharp1 = new[] { 11, 16, 52, 21, 8, 8 };
var csharp2 = csharp1.Distinct();
csharp1.SequenceEquals(csharp2);//Falseを返す
```

## 集合

### Union(指定したシーケンスとの和集合を返す)

```csharp
var first = new[] { 11, 16, 52, 21, 8, 8 };
var second = new[] { 16, 21, 20, 3 };
first.Union(second); //11,16,52,21,8,20,3を返す
```

### Except(指定したシーケンスとの差集合を返す)

```csharp
var first = new[] { 11, 16, 52, 21, 8, 8 };
var second = new[] { 16, 21, 20, 3 };
first.Except(second);    //11,52,8を返す
```

### Intersect(指定したシーケンスとの積集合を返す)

```csharp
var first = new[] { 11, 16, 52, 21, 8, 8 };
var second = new[] { 16, 21, 20, 3 };
first.Intersect(second); 16,21//を返す
```

## ソート

### OrderBy(昇順にソートしたシーケンスを返す)

```csharp
var csharp = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
csharp.OrderBy(x => x.Age)
//Groovy、Scala、C#、Javaの順で「シーケンス」を返す
```

### OrderByDescending(降順にソートしたシーケンスを返す)

```csharp
var csharp = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
csharp.OrderByDescending(x => x.Age);
//Java、C#、Groovy、Scalaの順で「シーケンス」を返す
```

### ThenBy(ソートして、キーが等しいシーケンスを更に昇順)

```csharp
var csharp = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
csharp.OrderBy(x => x.Age).ThenBy(x => x.Name);
//Groovy,Scala,C#,Javaの順で「シーケンス」を返す
```

### ThenByDescending(ソートして、キーが等しいシーケンスを更に降順)

```csharp
var csharp = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
csharp.OrderBy(x => x.Age).ThenByDescending(x => x.Name);
//Scala,Groovy,c#,Javaの順で「シーケンス」を返す
```

### Reverse(逆順にソートしたシーケンスを返す)

```csharp
var csharp = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
csharp.Reverse();
//Scala,Groovy,Java,C#の順で「シーケンス」を返す
```

## 射影

### Select(一つの要素を単一の要素に射影)

```csharp
var csharp = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
csharp.Select(x => x.Name());
//{C#,JAVA,Groovy,Scala}が返る
```

### SelectMany(一つの要素を単一の要素に射影。それをシーケンスにして返す)

```csharp
var csharp = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
csharp.SelectMany(x => X.Name.ToCharArray());
//{C, #, J, a, v, a, G, r, o, o, v, y, S, c, a, l, a}が返る
```

### GroupBy(指定のキーで要素をグループ化。その「キーとグループ」のシーケンスを返す)

```csharp
var csharp = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
csharp.GroupBy(x => x.age);
// -> {Key=11, csharp={{ Name = C#, Age = 11 }},
//     Key=16, csharp={{ Name = Java, Age = 16 }},
//     Key=8, csharp={{ Name = Groovy, Age = 8 }, { Name = Scala, Age = 8 }}}
//が返る
```

## 結合

### Join(内部結合を行ったシーケンスを返す)

```csharp
var outer = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
var inner = new[] {
    new{Name = "C#", DesignedBy = "Microsoft"},
    new{Name = "Java", DesignedBy = "Sun Microsystems"},
    new{Name = "Java", DesignedBy = "Oracle"},
};

outer.Join(inner, o => o.Name, i => i.Name,
            (o, i) => new { o.Name, o.Age, i.DesignedBy});
// -> {{ Name = C#, Age = 11, DesignedBy = Microsoft },
//     { Name = Java, Age = 16, DesignedBy = Sun Microsystems },
//     { Name = Java, Age = 16, DesignedBy = Oracle }}
```

### GroupJoin(左外部結合して、指定のキーでグループ化。その「キーとグループ」のシーケンスを返す)

```csharp
var outer = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
var inner = new[] {
    new{Name = "C#", DesignedBy = "Microsoft"},
    new{Name = "Java", DesignedBy = "Sun Microsystems"},
    new{Name = "Java", DesignedBy = "Oracle"},
};

outer.GroupJoin(inner,o => o.Name, i => i.Name,
            (o, i) => new { o.Name, o.Age, DesigndBy = i.Select(e => e.DesignedBy)});
// -> {{ Name = C#, Age = 11, DesigndBy = {Microsoft} },
//     { Name = Java, Age = 16, DesigndBy = {Sun Microsystems, Oracle} },
//     { Name = Groovy, Age = 8, DesigndBy = {} },
//     { Name = Scala, Age = 8, DesigndBy = {} }}
```

### Concat(２つのシーケンスを結合)

```csharp
var outer = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
var outer2 = new[] {
     new{Name = "Python", Age = 21},
     new{Name = "COBOL", Age = 52},
};

outer.Concat(outer2);
//  {{ Name = C#, Age = 11 },
//  { Name = Java, Age = 16 },
//  { Name = Groovy, Age = 8 },
//  { Name = Scala, Age = 8 },
//  { Name = Python, Age = 21 },
//  { Name = COBOL, Age = 52 }}
```

### Zip(指定した関数で2つのシーケンスを１つにマージ)

```csharp
var outer = new[] {
    new{Name = "C#", Age = 11},
    new{Name = "Java", Age = 16},
    new{Name = "Groovy", Age = 8},
    new{Name = "Scala", Age = 8},
};
var outer2 = new[] {
     new{Name = "Python", Age = 21},
     new{Name = "COBOL", Age = 52},
};

outer.Zip(outer2, (o1, o2) => o1.Name + "&" + o2.Name);
//  {C#&Python, Java&COBOL}
```

## 変換

|メソッド名|機能|
|:--|:--|
OfType|各要素を指定した型に変換します。<BR>キャストできない要素は除外します。
Cast|各要素を指定した型に変換します。<BR>キャストできない要素が含まれていた場合、例外をスローします。
ToArray|配列を作成します。
ToDictionary|連想配列(ディクショナリ)を作成します。
ToList|リストを生成します。
ToLookup|キーコレクション*1を生成します。
AsEnumerable|IEnumerable<T> を返します。*2
