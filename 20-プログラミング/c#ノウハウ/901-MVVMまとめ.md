# MVVMについて

## MVVMのイメージ

### 基本のパターン

```mermaid
    graph TD

            subgraph VIEWMODEL
                a[Icommand]
                b[Messenger]
            end
            subgraph VIEW
                c[ボタン]
                d[ダイアログ]
            end
            c--押したよ-->a
            b--表示して-->d
            InotifyProperty
```

### 〇のパターン

![](/tmp/800001-MVVM-2.png)

### ×のパターン

![](/tmp/800001-MVVM-3.png)

## VIEWについて

### VIEWはUI要素と結合したプレゼンテーションロジックを担当

- ==VIEWのコードビハインドはinitializeComponentのみであるべき。==
    - コードビハインドがどうしも必要なパターンはほぼ皆無

- XAMLだけで完結できない部分は「ビヘイビア」、「トリガー」、「アクション」で対処できるらしい
>ビヘイビアはトリガー＋アクション

## VIEWMODELについて

### VIEWMODELはUI要素と結合しないプレゼンテーションロジックを担当

- 下記責務を持つ
    1. ①Modelの公開するステートをラップ
    2. エンティティをVIEWに表示できるように整形
    3. Viewに公開

- ViewModelはViewにバインドされ、そのプロパティをViewに公開する。
  - ViewModelでプロパティ値の変更があった場合、INotifyPropertyChangedインターフェイスの実装を通じて、Viewに値の変更を通知してやる必要がある
  - つまり、==VIEWMODELのプロパティは特定のコントロールと結合してさえいなければ表示専用でもよい==

- ビジネスドメインに属するステートをViewModelに置くのは**禁止**
- 通常、最低一つの画面に一つのVIEWMODELが必要
- 更に、各コントトールが操作を持つならそれぞれにViewModelが要る！   （MODELから受け取ったものを表示するには大抵加工が要るから）

## VIEWとVIEWMODEL間のやり取り

- 恒常的に表示するものはVIEWODELがプロパティを持ち、VIEWがそれを見る
- VIEWで操作した時に起きるイベントは「コマンド」で処理
- ダイアログとか一時的に表示するものはVIEWMODELにプロパティ持たないので「MESSENGER」を経由する

### コマンド

**VIEW⇒VIEWMODEL**
- VIEWMODELがVIEWにデータ・バインディング使って公開する操作のこと。
    ==ICommandインターフェイスで、ViewModelのプロパティとして公開==される。

- 通常、コマンドは「メソッド」と、「そのメソッドが実行可能かを判断するメソッド」を含めて公開する。
    （falseを返したら、バインドされてるボタンが押せなくなるとか）
    ==**これによって、本当の意味で「（メソッドでなく）操作の公開」となる**==

- IComandインターフェイスのメンバ
    - void Execute(object parameter)
        ⇒処理する実体
    - bool CanExecute(object parameter)
        ⇒処理が可能かの判定
    - event EventHandler CanExecuteChanged;
        ⇒CanExecuteが変わった時に変更を通知する
    ==※いちいち実装が面倒くさいのでDelegateCommandを使う==
    delegameCommandとは。。。
    ==⇒判定(Func)してOKなら処理実行(Action)するインスタンス作ってくれる==



### Messenger

**VIEWMODEL⇒VIEW**

- ViewModelからViewを、データ・バインディングを使って操作すること 
    1. VIEWは予めMessengerをバインドして監視してる
    2. VIEWMODELはダイアログとか出したいときはMessengerからメッセージイベントを発行する
    3.VIEWは発行されたメッセージに対応したアクションを行える

>Messengerを使う理由
>>VIEWが表示する情報はVIEWMODELがプロパティとしてもってる。
>>ただ、ダイアログとか一時的なものまでVIEWMODELで持つのは自然じゃない。そういうものに対してMESSENGER使う

![test](/tmp/800001-MVVM.jpg)

==ただ、VIEWでMessengerの部分をコードビハインドはかっこ悪いので、そこはBehavorつかう。つまりMessenger+Behavorがきれい==

## MODELについて

### MODELはビジネスロジックを受け持つ。

- MODELはビジネスロジックを受け持つが、==「ステートフル」である==ことが特徴
    - WEBシステムなどは「ある処理から次の処理の間に保持する情報(ステート)はない」
    - クラサバのCLと違って、VIEWMODELはビジネス層のステートを持つからクラサバでもない
- MODELはステートフルなので、MODEL操作はMODELのステートのプロパティ変更となる。
⇒==MODELは変更通知機能を持つ==(INotifyPropertyChangedインターフェイズ)

### VIEWMODEL側から見えるMODELのインターフェイス

- Modelのステートの公開とその変更通知
- Modelの操作のための戻り値のないメソッド


## その他

### 入力値の検証について

- MODELの入力値の検証⇒MODEL
- VIEWの入力値の検証⇒VIEWMODEL
   ※エラー表示の文字はVIEWが管理すべきなので、マスタ用意してキーをMODELで指定する

# MaterialDesign使い方

## App.Xaml

```
    <Application.Recsharps>
        <RecsharpDictionary>
            <RecsharpDictionary.MergedDictionaries>
                <RecsharpDictionary csharp="pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml" />
                <RecsharpDictionary csharp="pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Defaults.xaml" />
            </RecsharpDictionary.MergedDictionaries>
        </RecsharpDictionary>
    </Application.Recsharps>


```

## MainWindowに下記を追加
```
        xmlns:materialDesign="http://materialdesigninxaml.net/winfx/xaml/themes"
        TextElement.Foreground="{DynamicRecsharp MaterialDesignBody}"
        TextElement.FontWeight="Regular"
        TextElement.FontSize="13"
        TextOptions.TextFormattingMode="Ideal" 
        TextOptions.TextRenderingMode="Auto"        
        Background="{DynamicRecsharp MaterialDesignPaper}"
        FontFamily="{DynamicRecsharp MaterialDesignFont}"