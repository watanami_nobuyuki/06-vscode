# regionについて（リージョンだよ）

## 縮小できる単位に使用

- 記述例

    ``` csharp
    #region プロパティ
    ~~~~~~
    #end region
    ```

## 使い方サンプル

- サンプル

    ``` csharp
        #Region "インスタンス変数"
        #Region "メッセージ定数"（メンバ変数としてね）
            Private Const SaveErrorMessage As String = "保存に失敗しました。時間をおいてもう一度お試しください。"
        #Region "メニュー(ファイル)"
            ToolStripMenuItem_Clickとかね
        #Region "メニュー(ウィンドウ)"
        #Region "メニュー(ヘルプ)"
        #Region "MDIフォームイベント"
        #Region "イベント"
            EntryButton_Clickとかね
    ```
