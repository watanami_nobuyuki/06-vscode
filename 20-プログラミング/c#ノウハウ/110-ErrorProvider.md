# ErrorProvider

- 入力値チェックを行って、エラーがあったら！マークを出せる

## 使い方

1. ツールボックスからErrorProviderをポトペタ
2. エラーチェックしたいコントロールのイベントで「validating」を選択
3. 下記ソース

    ``` csharp
        private void uxTableFilterText_Validating(object sender, CancelEventArgs e) {
            try {
            // 入力値を数値に変換する
            int iAge = int.Parse(uxTableFilterText.Text);
            // ★★★正常に整数に変換できた場合はエラーをクリアする★★★
            errorProvider1.SetError(uxTableFilterText, "");
        } catch (Exception) {
            e.Cancel = true;
            // ★★★例外が発生したのでエラーを表示する★★★
            errorProvider1.SetError(uxTableFilterText, "整数値以外の文字が入力されました！！");
        }
    }
    ```