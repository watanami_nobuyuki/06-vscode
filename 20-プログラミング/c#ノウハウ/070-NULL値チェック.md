# NULL値チェック（C#6.0）

## パターン１

- 今までの書き方

    ``` CSharp
    if (Name != null) {
        List.add(Name);
    }
    ```

- 新しい書き方

    ``` CSharp
    List?.add(name);
    ```

## パターン2

- 今までの書き方

    ``` CSharp
    if (value1 == null){
        Console.WriteLine("Null");
    }else{
        Console.WriteLine(value1);
    }
    ```

- 新しいの

    ``` CSharp
    Console.WriteLine(Value1 ?? "Null)";
    ```

## パターン3

- 今までの書き方

    ``` csharp
        value1=null
        if (value1 == null){
            value1="ヌルよ";
        }
    ```

- 新しいの

    ``` CSharp
        value1=null
        value1 = value1 ?? "ヌルよ"
    ```