# ACCESSに限って言えば

- DAOが一番早い！

1. プロジェクト→参照の追加「DAO」で検索
    1. 32bit：Microsoft DAO X.X Object Libraryを追加
    2. 64bit：Microsoft Office XX.X Access Database Engine Object Libraryを追加
2. ソリューションエクスプローラでDAOのプロパティで「相互運用型の埋め込み」をFALSEに
